use super::export::*;
use super::mesh_assembly::*;
use super::miscellaneous::*;
use super::ring_assembly::*;
use super::assembler::*;

use std::fs;

#[test]
fn it_works() {
    assert_eq!(2 + 2, 4);
}

#[test]
fn make_cube() {
    let bottom = create_ring_default_up(4, V::new(0.0, 0.0, 0.0), V::new(0.0, 0.0, 1.0));
    let top = bottom.clone();
    let top = translate(&top, V::new(0.0, 0.0, 1.0));
    let res = assemble_mesh(&vec![bottom, top], true, true);
    let obj = export_flat_to_obj(&res, &compute_normals(&res));

    fs::write("cube.obj", obj).expect("unable to save file");
}

#[test]
fn make_cube2() {
    let sample = create_ring_default_up(4, V::new(0.0, 0.0, 0.0), V::new(0.0, 0.0, 1.0));
    let bottom = resample_ring_uniform(&sample, 12, false);
    let mut rings = vec![bottom];
    for i in 1..=4 {
        let ring = translate_random(
            &translate(&rings[0], V::new(0.0, 0.0, 0.25 * i as F)),
            V::new(-0.12, -0.12, -0.12),
            V::new(0.12, 0.12, 0.12),
            &mut rand::thread_rng(),
        );
        rings.push(ring);
    }
    rings.push(scale_from_centroid(rings.last().unwrap(), 0.5));
    let res = assemble_mesh(&rings, true, true);
    let obj = export_flat_to_obj(&res, &compute_normals(&res));

    fs::write("cube2.obj", obj).expect("unable to save file");
}

#[test]
fn make_starfish() {
    let ring = create_ring_default_up(10, V::new(0.0, 0.0, 0.0), V::new(0.0, 1.0, 0.0));
    let mut lower = translate(&ring, V::new(0.0, 0.01, 0.0));
    let mut middle = scale_from_centroid(&translate(&lower, V::new(0.0, 0.3, 0.0)), 1.2);
    let mut top = translate(&lower, V::new(0.0, 0.5, 0.0));
    let closure = translate(&scale_from_centroid(&top, 0.9), V::new(0.0, -0.1, 0.0));

    for index in 0..lower.len() {
        let scale = if index % 2 == 0 { 1.0 } else { 3.0 };
        lower[index] *= scale;
        middle[index] *= scale;
        top[index] *= scale;
    }
    let mesh = assemble_mesh(&vec![lower, middle, top, closure], true, true);
    let obj = export_flat_to_obj(&mesh, &compute_normals(&mesh));

    fs::write("starfish.obj", obj).expect("unable to save file");
}

#[test]
fn make_ruring() {
    let ring = create_ring_default_up(21, V::new(0.0, 0.0, 0.0), V::new(0.0, 1.0, 0.0));
    let mut lower = translate(&ring, V::new(0.0, 0.01, 0.0));
    let mut middle = scale_from_centroid(&translate(&lower, V::new(0.0, 0.3, 0.0)), 1.2);
    let mut top = translate(&lower, V::new(0.0, 0.5, 0.0));
    let closure = translate(&scale_from_centroid(&top, 0.9), V::new(0.0, -0.1, 0.0));

    for index in 0..lower.len() {
        let scale = if index % 3 == 0 { 2.0 } else { 3.0 };
        lower[index] *= scale;
        middle[index] *= scale;
        top[index] *= scale;
    }
    let mesh = assemble_mesh(&vec![lower, middle, top, closure], true, true);
    let obj = export_flat_to_obj(&mesh, &compute_normals(&mesh));

    fs::write("ruring.obj", obj).expect("unable to save file");
}

#[test]
fn make_gear() {
    let size: usize = 26;
    let a = Assembler::new(size).scale(5.0).
        add(create_ring_default(size * 2)).scale(6.5).resample(size * 2, true);
    let mut outer = vec![];
    for index in (0..a.rings[0].len()).step_by(2) {
        outer.push(a.rings[0][index]);
        outer.push(a.rings[1][index * 2]);
        outer.push(a.rings[1][index * 2 + 1]);
        outer.push(a.rings[0][index + 1]);
    }

    let gear = Assembler::new(size).scale(3.0).
        add_clone_after(2.0).
        add(outer).translate_in_dir(2.0).
        add_clone_after(-2.0).
        add(create_ring_default(size * 2)).scale(5.0).
        add(create_ring_default(size)).scale(3.0).
        assemble(false, false);

    let obj = export_flat_to_obj(&gear, &compute_normals(&gear));

    fs::write("gear.obj", obj).expect("unable to save file");
}

