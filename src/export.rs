use super::miscellaneous::*;

pub fn export_flat_to_obj(vertices: &[V], normals: &[V]) -> String {
    let mut result = String::new();

    for v in vertices {
        result += &format!("v {} {} {} \n", v.x, v.y, v.z);
    }

    for n in normals {
        result += &format!("vn {} {} {} \n", n.x, n.y, n.z);
    }

    for f in (0..vertices.len()).step_by(3) {
        result += &format!(
            "f {}//{} {}//{} {}//{} \n",
            f + 1,
            f + 1,
            f + 2,
            f + 2,
            f + 3,
            f + 3
        );
    }

    return result;
}
