use super::miscellaneous::*;

pub fn assemble_mesh(rings: &[Vec<V>], close_bottom: bool, close_top: bool) -> Vec<V> {
    let mut result = vec![];
    if close_bottom {
        result.extend(close_ring(&rings[0], false));
    }
    for index in 0..rings.len() - 1 {
        result.extend(connect_rings(&rings[index], &rings[index + 1]));
    }
    if close_top {
        result.extend(close_ring(&rings[rings.len() - 1], true));
    }
    return result;
}

pub fn compute_normals(mesh: &[V]) -> Vec<V> {
    let mut result = vec![];
    for index in (0..mesh.len()).step_by(3) {
        let normal = compute_triangle_normal(mesh[index], mesh[index + 1], mesh[index + 2]);
        result.push(normal);
        result.push(normal);
        result.push(normal);
    }
    result
}

fn connect_rings(ring_one: &[V], ring_two: &[V]) -> Vec<V> {
    let centroid_one = centroid_space(ring_one);
    let centroid_two = centroid_space(ring_two);
    let dir = centroid_two - centroid_one;

    let mut index_one: i32 = 0;
    //  align ring two to best position to one
    let mut index_two: i32 = 0;
    let mut min_dis = distance_to_line(
        ring_one[indexize(index_one, ring_one.len())],
        ring_two[indexize(index_two, ring_two.len())],
        dir,
    );
    for index in 1..ring_two.len() {
        let local_min = distance_to_line(
            ring_one[indexize(index_one, ring_one.len())],
            ring_two[index],
            dir,
        );
        if local_min < min_dis {
            min_dis = local_min;
            index_two = index as i32;
        }
    }

    let mut steps_from_one = 0;
    let mut steps_from_two = 0;
    let mut result = vec![];

    while steps_from_one + steps_from_two < ring_one.len() + ring_two.len() {
        let take_from_one: bool;
        if steps_from_one >= ring_one.len() {
            take_from_one = false;
        } else if steps_from_two >= ring_two.len() {
            take_from_one = true;
        } else {
            take_from_one = !(steps_from_one as F / ring_one.len() as F
                > steps_from_two as F / ring_two.len() as F);
        }
        if take_from_one {
            result.push(ring_one[indexize(index_one, ring_one.len())]);
            result.push(ring_one[indexize(index_one + 1, ring_one.len())]);
            result.push(ring_two[indexize(index_two, ring_two.len())]);
            steps_from_one += 1;
            index_one += 1;
        } else {
            result.push(ring_two[indexize(index_two + 1, ring_two.len())]);
            result.push(ring_two[indexize(index_two, ring_two.len())]);
            result.push(ring_one[indexize(index_one, ring_one.len())]);
            steps_from_two += 1;
            index_two += 1;
        }
    }
    return result;
}

fn choose_next_in_one(v1: V, v2: V, one: V, two: V) -> bool {
    let a1 = compute_triangle_area_3d(v1, v2, one);
    let a2 = compute_triangle_area_3d(v1, v2, two);
    a1 < a2
}

fn close_ring(ring: &[V], front: bool) -> Vec<V> {
    let mut index_one: i32 = 0;
    let mut index_two: i32 = 1;
    let mut result = vec![];

    for _edge in 0..ring.len() - 2 {
        let next_in_one = choose_next_in_one(
            ring[index_one as usize],
            ring[index_two as usize],
            prev(ring, index_one),
            next(ring, index_two),
        );
        if next_in_one {
            result.push(ring[indexize(index_one - 1, ring.len())]);
            result.push(ring[indexize(index_one, ring.len())]);
            result.push(ring[indexize(index_two, ring.len())]);
            index_one = indexize(index_one - 1, ring.len()) as i32;
        } else {
            result.push(ring[indexize(index_one, ring.len())]);
            result.push(ring[indexize(index_two, ring.len())]);
            result.push(ring[indexize(index_two + 1, ring.len())]);
            index_two = indexize(index_two + 1, ring.len()) as i32;
        }
    }

    if !front {
        result.reverse();
    }
    return result;
}
