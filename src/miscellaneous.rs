use cgmath::prelude::*;
use rand::Rng;

pub type V = cgmath::Vector3<f32>;
pub type F = f32;

pub fn indexize(index: i32, size: usize) -> usize {
    let s = size as i32;
    let mut res = index;
    while res < 0 {
        res += s as i32;
    }
    (res % s) as usize
}

pub fn next(vec: &[V], index: i32) -> V {
    vec[indexize(index + 1, vec.len())]
}

pub fn prev(vec: &[V], index: i32) -> V {
    vec[indexize(index - 1, vec.len())]
}

pub fn centroid_space(vertices: &[V]) -> V {
    vertices.iter().sum::<V>() / vertices.len() as F
}

pub fn min_seq_distance(vec: &[V]) -> F {
    let mut min_val = vec[0].distance(vec[vec.len() - 1]);
    for index in 0..vec.len() - 1 {
        min_val = min_val.min(vec[index].distance(vec[index + 1]));
    }
    min_val
}

pub fn distance_to_line(vertex: V, start: V, direction: V) -> F {
    let d = direction.normalize();
    let v = vertex - start;
    let t = v.dot(d);
    let point_on_line = start + d * t;
    vertex.distance(point_on_line)
}

pub fn compute_circumcircle_radius(a: F, b: F, c: F) -> F {
    a * b * c / F::sqrt((a + b + c) * (a + b - c) * (a + c - b) * (b + c - a))
}

pub fn compute_triangle_area(a: F, b: F, c: F) -> F {
    //Herons formula
    let half_perimeter = (a + b + c) / 2.0;
    let area_squared =
        F::abs(half_perimeter * (half_perimeter - a) * (half_perimeter - b) * (half_perimeter - c));
    F::sqrt(area_squared)
}

pub fn compute_triangle_area_3d(a: V, b: V, c: V) -> F {
    compute_triangle_area(a.distance(b), b.distance(c), a.distance(c))
}

pub fn compute_triangle_normal(v0: V, v1: V, v2: V) -> V {
    (v0 - v1).normalize().cross(v0 - v2).normalize()
}

pub fn compute_ring_normal(vertices: &[V]) -> V {
    assert!(vertices.len() >= 3);
    let center = centroid_space(vertices);
    let mut first = vertices[0] - center;
    let mut normal = V::zero();
    for index in 1..vertices.len() {
        let next = vertices[index] - center;
        normal += first.cross(next);
        first = next;
    }
    normal
}

pub fn create_vector_alignment_matrix(vector: V, target_vector: V) -> cgmath::Matrix3<F> {
    let start = vector.normalize();
    let end = target_vector.normalize();

    if start.distance(end) < 1e-4 {
        cgmath::Matrix3::from_scale(1 as F)
    } else {
        let axis = start.cross(end).normalize();
        let cos = start.dot(end);
        let angle = cgmath::Rad::acos(cos);
        cgmath::Matrix3::from_axis_angle(axis, angle)
    }
}

pub fn translate(vertices: &[V], factor: V) -> Vec<V> {
    vertices
        .iter()
        .map(|v| v.add_element_wise(factor))
        .collect()
}

pub fn scale(vertices: &[V], center: V, factor: F) -> Vec<V> {
    vertices
        .iter()
        .map(|v| (v.sub_element_wise(center) * factor).add_element_wise(center))
        .collect()
}

pub fn scale_from_centroid(vertices: &[V], factor: F) -> Vec<V> {
    scale(vertices, centroid_space(vertices), factor)
}

// x towards the center, y right, z up
pub fn translate_random<R: Rng>(vertices: &[V], min: V, max: V, rng: &mut R) -> Vec<V> {
    assert!(vertices.len() > 0);
    let center = centroid_space(vertices);
    let orig_pos_of_first = vertices[0];
    let mut res = vertices.iter().map(|x| x.clone()).collect::<Vec<V>>();
    for index in 0..vertices.len() {
        let forward = (center - vertices[index]).normalize();
        let pos_of_next = if index + 1 < vertices.len() {
            vertices[index + 1]
        } else {
            orig_pos_of_first
        };
        let right = (pos_of_next - vertices[index]).normalize();
        let up = right.cross(forward).normalize();
        let rand_offset = V::new(
            rng.gen_range(min.x,max.x),
            rng.gen_range(min.y,max.y),
            rng.gen_range(min.z,max.z),
        );
        res[index] += forward * rand_offset.x + right * rand_offset.y + up * rand_offset.z;
    }
    res
}

// counterclockwise in the axis direction
pub fn rotate(vertices: &[V], axis: V, center: V, degrees: cgmath::Deg<F>) -> Vec<V> {
    let matrix = cgmath::Matrix3::from_axis_angle(axis, degrees);
    vertices
        .iter()
        .map(|v| {
            let r = v - center;
            let r = matrix * r;
            r + center
        })
        .collect()
}

pub fn rotate_in_centroid(vertices: &[V], axis: V, degrees: cgmath::Deg<F>) -> Vec<V> {
    rotate(vertices, axis, centroid_space(vertices), degrees)
}

pub fn rotate_to_align(vertices: &[V], center: V, vector: V, target_vector: V) -> Vec<V> {
    let matrix = create_vector_alignment_matrix(vector, target_vector);
    vertices
        .iter()
        .map(|v| {
            let r = v - center;
            let r = matrix * r;
            r + center
        })
        .collect()
}

pub fn rotate_to_align_centroid(vertices: &[V], vector: V, target_vector: V) -> Vec<V> {
    rotate_to_align(vertices, centroid_space(vertices), vector, target_vector)
}
