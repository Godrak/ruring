use super::prelude::*;
use cgmath::InnerSpace;

pub struct Assembler {
    pub rings: Vec<Vec<V>>,
}

impl Assembler {
    pub fn new(init_size: usize) -> Self {
        Assembler::new_custom(create_ring_default(init_size))
    }

    pub fn new_custom(ring: Vec<V>) -> Self {
        Assembler {
            rings: vec![ring]
        }
    }

    pub fn scale(self, factor: f32) -> Self {
        let n = scale_from_centroid(self.rings.last().unwrap(), factor);
        self.swap_last(n)
    }

    pub fn translate(self, factor: V) -> Self {
        let n = translate(self.rings.last().unwrap(), factor);
        self.swap_last(n)
    }

    pub fn resample(self, size: usize, shift: bool) -> Self {
        let n = resample_ring_uniform(self.rings.last().unwrap(), size, shift);
        self.swap_last(n)
    }

    pub fn translate_in_dir(self, distance: f32) -> Self {
        let n = translate(self.rings.last().unwrap(),
                          distance * compute_ring_normal(self.rings.last().unwrap().as_slice()).normalize());
        self.swap_last(n)
    }

    pub fn add_clone_after(self, distance: f32) -> Self {
        self.add_clone().translate_in_dir(distance)
    }

    pub fn add_clone(self) -> Self {
        let last = self.get_last();
        self.add(last.clone())
    }

    pub fn get_last(&self) -> Vec<V> {
        return self.rings.last().unwrap().clone();
    }

    pub fn add(mut self, ring: Vec<V>) -> Self {
        self.rings.push(ring);
        self
    }

    fn swap_last(mut self, ring: Vec<V>) -> Self {
        self.rings.pop();
        self.rings.push(ring);
        self
    }

    pub fn assemble(&self, close_bottom: bool, close_top: bool) -> Vec<V> {
        assemble_mesh(self.rings.as_slice(), close_bottom, close_top)
    }
}