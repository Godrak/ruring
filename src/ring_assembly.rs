use super::miscellaneous::*;
use cgmath::prelude::*;

//CCW rings are front facing!

//creates perfect circular ring with the given vertices count, rotated so that the first vertex defines direction from the centre to the up vector,
//normal defines the ring normal, and position defines the centre position
pub fn create_ring(size: usize, position: V, normal: V, up: V) -> Vec<V> {
    let mut ring = vec![];
    for index in 0..size {
        let x = F::cos(
            2.0 * std::f32::consts::PI * index as F / size as F + std::f32::consts::FRAC_PI_2,
        );
        let y = F::sin(
            2.0 * std::f32::consts::PI * index as F / size as F + std::f32::consts::FRAC_PI_2,
        );
        ring.push(V::new(x, y, 0.0));
    }
    let current_normal = V::new(0.0, 0.0, 1.0);
    let current_up = V::new(0.0, 1.0, 0.0);
    let up_rot_matrix = create_vector_alignment_matrix(current_up, up);
    for v in ring.iter_mut() {
        *v = up_rot_matrix * *v;
    }

    let current_normal = up_rot_matrix * current_normal;

    let rot_matrix = create_vector_alignment_matrix(current_normal, normal);

    for v in ring.iter_mut() {
        *v = rot_matrix * *v;
    }

    translate(&ring, position)
}

pub fn create_ring_default_up(size: usize, position: V, normal: V) -> Vec<V> {
    create_ring(size, position, normal, V::new(0.0, 1.0, 0.0))
}

pub fn create_ring_default(size: usize) -> Vec<V> {
    create_ring(size, V::new(0.0, 0.0, 0.0), V::new(0.0, 0.0, 1.0), V::new(0.0, 1.0, 0.0))
}


pub fn resample_ring_uniform(ring: &[V], size: usize, shift: bool) -> Vec<V> {
    assert!(size > 0);
    assert!(ring.len() > 0);

    let mut total_distance = 0.0;
    let mut distances = vec![];
    let mut local_ring = vec![];

    for index in 0..ring.len() - 1 {
        distances.push(total_distance);
        local_ring.push(ring[index]);
        total_distance += ring[index].distance(ring[index + 1]);
    }

    distances.push(total_distance);
    local_ring.push(ring[ring.len() - 1]);
    total_distance += ring[0].distance(ring[ring.len() - 1]);
    distances.push(total_distance);
    local_ring.push(ring[0]);

    let sample_distance = total_distance / size as F;
    let mut resampled_distance = if shift { sample_distance / 2.0 } else { 0.0 }; // starting a little shifted, for nicer results
    let mut prev_vertex_index = 0;

    let merger = |v1: V, v2: V, f: F| v1 * (1.0 - f) + v2 * f;

    let mut result = vec![];
    while resampled_distance < total_distance {
        while resampled_distance > distances[prev_vertex_index + 1] {
            prev_vertex_index += 1;
        }
        result.push(merger(
            local_ring[prev_vertex_index],
            local_ring[prev_vertex_index + 1],
            (resampled_distance - distances[prev_vertex_index])
                / (distances[prev_vertex_index + 1] - distances[prev_vertex_index]),
        ));
        resampled_distance += sample_distance;
    }
    return result;
}

pub fn create_hermit_rings(
    layers_count: usize,
    layer_vertices_count: usize,
    start_pos: V,
    start_tangent: V,
    end_pos: V,
    end_tangent: V,
) -> Vec<Vec<V>> {
    let mut result: Vec<Vec<V>> = Vec::new();
    for index in 0..layers_count {
        let t = index as F / layers_count as F;
        let t2 = t * t;
        let t3 = t * t * t;
        let position = (2.0 * t3 - 3.0 * t2 + 1.0) * start_pos
            + (t3 - 2.0 * t2 + t) * start_tangent
            + (-2.0 * t3 + 3.0 * t2) * end_pos
            + (t3 - t2) * end_tangent;
        let normal = (6.0 * t2 - 6.0 * t) * start_pos
            + (3.0 * t2 - 4.0 * t + 1.0) * start_tangent
            + (-6.0 * t2 + 6.0 * t) * end_pos
            + (3.0 * t2 - 2.0 * t) * end_tangent;
        let ring = create_ring_default_up(layer_vertices_count, position, normal);
        result.push(ring);
    }
    return result;
}
