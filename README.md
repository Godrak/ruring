# Ruring

Rust library for procedural generation of 3D meshes. Allows defining meshes by connected 
rings of vertices. Ring is a vector of vertices, where each vector is considered 
connected to the previous and next. Additionaly, last vertex is connected to first.

Filling between rings is determined algorithmically, trying to make the 'best' 
transition. The result should always be closed shape, but intersecting or degenerated
triangles may occur.

If the result seems odd or not what you intended, it sometimes helps to
make the defining rings more dense. Last resort is to manually connect the troublesome
section.

## Cube example

    let bottom = create_ring_default_up(4, V::new(0.0, 0.0, 0.0), V::new(0.0, 0.0, 1.0));
    let top = bottom.clone();
    let top = translate(&top, V::new(0.0, 0.0, 1.0));
    let res = assemble_mesh(&vec![bottom, top], true, true);
    let obj = export_flat_to_obj(&res, &compute_normals(&res));

    fs::write("cube.obj", obj).expect("unable to save file");

![Cube](/img/cube.png)

It does not support branching, but there is still a lot of shapes
tha can be simply generated via this approach. There is also an Assembly helper class
which makes thing easier.

## Gear example

    fn make_gear() {
    let size: usize = 26; //ring size (number of vertices)
    
    // prepare outer ring by interleaving two rings of different size
    let a = Assembler::new(size).scale(5.0).
        add(create_ring_default(size * 2)).scale(6.5).resample(size * 2, true);
    let mut outer = vec![];
    for index in (0..a.rings[0].len()).step_by(2) {
        outer.push(a.rings[0][index]);
        outer.push(a.rings[1][index * 2]);
        outer.push(a.rings[1][index * 2 + 1]);
        outer.push(a.rings[0][index + 1]);
    }

    let gear = Assembler::new(size).scale(3.0).         //inner ring 1 (inner gear edge)
        add_clone_after(2.0).                           // innter ring 2, thickness of gear is 2.0
        add(outer).translate_in_dir(2.0).               //outer ring 2 (outer gear edge, with teeth)
        add_clone_after(-2.0).                          // outer ring dd1
        add(create_ring_default(size * 2)).scale(5.0). // middle ring, helps to corrent the filling algorithm, not visible
        add(create_ring_default(size)).scale(3.0).  //innter ring 1 again, to close the shape
        assemble(false, false);

    let obj = export_flat_to_obj(&gear, &compute_normals(&gear));

    fs::write("gear.obj", obj).expect("unable to save file");
}

![Gear](/img/gear.png)
